﻿using System;

namespace PositiveAndNegativeACKs
{
    public class ReliabilityLayer
    {
        public static readonly Byte ACK = 0x06, NAK = 0x0e; // static codes for ACK and NACK
        private static readonly int[] mask = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 }; // a mask to separate a single bit out of byte

        // Verifies message with CRC8
        public static bool VerifyCRC8 (Byte[] msg)
        {
            return CalculateCRC8(msg) == 0; // if register is not 0 in the end, message has a bit error
        }


        public static Byte[] AddCRC8(Byte[] msg)
        {
            Byte[] newMsg = new Byte[msg.Length + 1];
            Array.Copy(msg, newMsg, msg.Length);

            newMsg[msg.Length] = CalculateCRC8(newMsg);

            return newMsg;
        }

        static Byte CalculateCRC8(Byte[] data)
        {
            Byte register = 0; // CRC shift register

            for (int i = 0; i < data.Length; i++)
            {
                for (int j = 7; j >= 0; j--)
                {
                    int pos2 = ((register & mask[7]) >> 7) ^ ((register & mask[1]) >> 1);   // pos2 = pos7 XOR pos1
                    int pos1 = ((register & mask[7]) >> 7) ^ (register & mask[0]);          // pos1 = pos7 XOR pos0
                    int pos0 = ((register & mask[7]) >> 7) ^ ((data[i] & mask[j]) >> j);     // pos0 = pos7 XOR (next bit in message)

                    register = (Byte)(register << 1); // moves all the bits in the register to the left by one

                    // sets new bits in place
                    register = (Byte)((register & ~mask[2]) | (pos2 << 2));
                    register = (Byte)((register & ~mask[1]) | (pos1 << 1));
                    register = (Byte)((register & ~mask[0]) | (pos0));
                }
            }

            return register;
        }

        public static void ColoredMessage(string txt, ConsoleColor c = ConsoleColor.White)
        {
            ConsoleColor orginal = Console.ForegroundColor;
            Console.ForegroundColor = c;
            Console.WriteLine(txt);
            Console.ForegroundColor = orginal;
        }

        // mode 0 = using both ACKs (default), 1 = using only positive ACKS, 2 = using only negative ACKS
        public static int AskForMode()
        {
            Console.WriteLine("Tpe in mode\n(0 = using both ACKs (default), 1 = using only positive ACKS, 2 = using only negative ACKS):");
            string input;
            int mode = 0;
            if ((input = Console.ReadLine()) != "") try { mode = int.Parse(input); } catch { }
            return mode;
        }
    }
}
