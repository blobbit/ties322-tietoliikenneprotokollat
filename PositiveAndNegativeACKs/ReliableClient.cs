﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace PositiveAndNegativeACKs
{
    public class ReliableClient : ReliabilityLayer
    {
        UdpClient socket;
        IPEndPoint server;
        int mode = 0; // 0 = using both ACKs (default), 1 = using only positive ACKS, 2 = using only negative ACKS

        public ReliableClient(int port_client, string ipserver, int port_server)
        {
            Console.Title = "Reliable Client";
            socket = new UdpClient(port_client);
            server = new IPEndPoint(IPAddress.Parse(ipserver), port_server);
            socket.Connect(server);
            mode = AskForMode();
        }

        public void Send(string message)
        {
            Byte[] data = AddCRC8(Encoding.ASCII.GetBytes(message));

            bool success = false;            

            // sends the packet until success
            while (!success)
            {
                ColoredMessage("Sending data...", ConsoleColor.Magenta);
                socket.Send(data, data.Length);

                switch (mode)
                {
                    case 0:
                        success = WaitBothACKs();
                        break;
                    case 1:
                        success = WaitOnlyPositiveACK();
                        break;
                    case 2:
                        success = WaitOnlyNegativeACK();
                        break;
                    default:
                        ColoredMessage("Client mode undefined - Error!", ConsoleColor.Red);
                        success = true;
                        break;
                }
            }
            ColoredMessage("Success!", ConsoleColor.Green);
        }

        bool WaitBothACKs()
        {
            Byte[] receivedBytes = socket.Receive(ref server);

            return receivedBytes[0] == ReliabilityLayer.ACK;
        }

        bool WaitOnlyPositiveACK()
        {
            socket.Client.ReceiveTimeout = 100; // timeout 100 ms

            try
            {
                Byte[] receivedBytes = socket.Receive(ref server);
                return true;
            } catch (SocketException)
            {
                ColoredMessage("Timeout - Error!", ConsoleColor.Red);
                return false;
            }
        }

        bool WaitOnlyNegativeACK()
        {
            return true;
        }

        public void Close()
        {
            socket.Close();
        }
    }
}
