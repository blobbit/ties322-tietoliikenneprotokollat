﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace PositiveAndNegativeACKs
{
    public class ReliableServer : ReliabilityLayer
    {
        VirtualSocket.VirtualSocket socket;
        IPEndPoint ep;
        int mode = 0; // 0 = using both ACKs (default), 1 = using only positive ACKS, 2 = using only negative ACKS

        public ReliableServer(VirtualSocket.VirtualSocket s, IPEndPoint ipep)
        {
            Console.Title = "Reliable Server";
            socket = s;
            ep = ipep;
            mode = AskForMode();
        }

        public string Receive()
        {
            while (true)
            {
                Byte[] receivedBytes = socket.Receive(ref ep);

                if (!VerifyCRC8(receivedBytes))
                {
                    ColoredMessage("Bit error detected !", ConsoleColor.Red);
                    if (mode != 1) socket.Send(new Byte[] { NAK }, 1, ep);
                    continue;
                }

                socket.Send(new Byte[] { ACK }, 1, ep);

                // Last char is CRC8 byte, so let's get rid of it
                string msg = Encoding.ASCII.GetString(receivedBytes);
                msg = msg.Remove(msg.Length - 1);
                return msg;
            }
        }
    }
}
