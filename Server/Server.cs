﻿using System;
using System.Net;
using PositiveAndNegativeACKs;

class Server
{
    static int port = 1337;

    static void Main(string[] args)
    {
        Console.Title = "Server";
        VirtualSocket.VirtualSocket socket = new VirtualSocket.VirtualSocket(port, 0, 0, 1);

        // endpoint listens to port
        IPEndPoint ep = new IPEndPoint(IPAddress.Any, port);

        ReliableServer server = new ReliableServer(socket, ep);

        Console.Clear();

        while (true) Console.WriteLine(server.Receive());
    }
}
