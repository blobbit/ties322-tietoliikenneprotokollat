﻿using System;
using System.Net;
using System.Text;
using PositiveAndNegativeACKs;

class Client
{
    static int PORT_CLIENT = 0, PORT_SERVER = 1337;
    static string IP_SERVER = "127.0.0.1";

    static void Main(string[] args)
    {
        Console.Title = "Testing Client";

        String input;

        Console.WriteLine("Type in client port (default " + PORT_CLIENT + "): ");
        if ((input = Console.ReadLine()) != "") try { PORT_CLIENT = int.Parse(input); } catch { }

        Console.WriteLine("Type in server port (default " + PORT_SERVER + "): ");
        if ((input = Console.ReadLine()) != "") try { PORT_SERVER = int.Parse(input); } catch { }



        ReliableClient client = new ReliableClient(PORT_CLIENT, IP_SERVER, PORT_SERVER);

        Console.Clear();

        while (true)
        {
            Console.Write("Send message: ");
            if ((input = Console.ReadLine()) == "") break;

            client.Send(input);
        }
            
        client.Close();
    }
}
