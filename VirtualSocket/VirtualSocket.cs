﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace VirtualSocket
{
    public class VirtualSocket : UdpClient
    {
        private int dropRate = 10, maxDelay = 0, maxBitErrors = 0;

        private static readonly int[] mask = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 }; // a mask to separate a single bit out of byte

        public VirtualSocket(int port, int dropRate = 10, int maxDelay = 1500, int maxBitErrors = 3) : base(port) {
            this.dropRate = dropRate;
            this.maxDelay = maxDelay;
            this.maxBitErrors = maxBitErrors;
        }

        public new byte[] Receive(ref IPEndPoint ep)
        {
            Random rng = new Random();

            while (true)
            {
                byte[] msg = base.Receive(ref ep); // Receives the message

                if (rng.Next(0,100) < dropRate) // Randomly drops the packet, static variable "dropRate" being the drop rate in percents
                    Console.WriteLine("Dropped packet");
                else
                {
                    Thread.Sleep(rng.Next(0,maxDelay)); // Randomly delays the packet (simulates the delay by stopping the thread), static variable "maxDelay" being the max delay in milliseconds

                    for (int i = rng.Next(0, maxBitErrors); i < maxBitErrors; i++) // Randomly creates bit errors on the packet, static variable "maxBitErrors" being the max amount of bit errors
                    {
                        
                        int byteIndex = rng.Next(0, msg.Length - 1); // Select random byte to create a bit error on

                        Byte b = msg[byteIndex];

                        int bitIndex = rng.Next(0, mask.Length - 1); // Select random bit to change

                        b = (Byte)(((int)b & ~mask[bitIndex]) ^ (1 << bitIndex)); // Replaces the bit in bitIndex with (xor) 1 
                        
                        msg[byteIndex] = b; // puts 'corrupted' byte back to the message
                    }

                    return msg;
                }
            }
        }
    }
}
