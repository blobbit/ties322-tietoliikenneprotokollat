using System;
using System.Net;
using System.Text;

namespace VirtualSocket
{
    class Program
    {
        static int port = 1337;

        static void Main(string[] args)
        {
            VirtualSocket socket = new VirtualSocket(port);

            bool listening = true;

            // endpoint listens to port
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, port);

            while (listening)
            {
                Byte[] receivedBytes = socket.Receive(ref ep);

                String msg = Encoding.ASCII.GetString(receivedBytes);

                Console.WriteLine(msg.Remove(msg.Length-1)); // writes out the message for testing purposes. last char is CR8 byte, so let's get rid of it
            }

            socket.Close();
        }
    }
}