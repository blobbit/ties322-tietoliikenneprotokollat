# TIES322 Tietoliikenneprotokollat 2

Repository contains one project for each category in "Reliability on top of UDP" section,
as well as a client and a server for testing purposes.

Course assignments:
http://users.jyu.fi/~arjuvi/opetus/ties322/2018/demot.html

## Assignments / Progress

### Virtual Socket (12p)

1. (2p) Make an application for testing purposes, e.g. client sends data to server and server displays it on screen. It is enough for the application to send data only in one direction (and later acknowledgements in the other). Use the test application to test what you implement later.
2. (2p) Make a virtual socket that randomly drops a packet (i.e. does not pass it to UDP socket or to the application)
3. (3p) Add to the virtual socket: randomly delays a packet, before passing it forward
4. (5p) Add to the virtual socket: randomly generates bit errors on the data of the packet, a single bit error is enough. 

### Positive and Negative ACKs (12p)

1. (2p) Add to your data packet some method for detecting at least a single bit error, e.g. CRC8, you can use possible built-in methods in programming languages.
2. (2p) Reliable data transfer with positive and negative ACKs